from flask_assets import Bundle

css_bundle = Bundle("styles.css", filters="cssmin", output="gen/%(version)s-styles.css")
js_bundle = Bundle("tracking.js", output="gen/%(version)s-scripts.js")


def jinja_should_autoescape(filename: str):
    """
    Returns `True` if autoescaping should be active for the given template name.
    """
    if filename is None:
        return False
    return filename.endswith(
        (
            ".html",
            ".htm",
            ".xml",
            ".xhtml",
            ".html.jinja",
            ".html.jinja2",
            ".xml.jinja",
            ".xml.jinja2",
            ".xhtml.jinja",
            ".xhtml.jinja2",
        )
    )
