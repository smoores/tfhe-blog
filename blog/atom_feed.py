from flask import render_template, request, escape
from dataclasses import dataclass
from bs4 import BeautifulSoup
from dateutil import parser
from pathlib import Path

import os
import datetime as dt
import time


TEMPLATES_DIR = f"{Path(__file__).parent.absolute()}/templates"
TZINFO = dt.time(time.timezone // 3600).strftime("+%H:%M")


@dataclass
class Post:
    title: str
    updated: str
    published: str
    path: str
    author: str
    content: str


def extract_post_attributes(filename: str):
    markup = render_template(f"posts/{filename}")
    soup = BeautifulSoup(markup, "html.parser")
    title = soup.article.h2.get_text()
    updated_dt = dt.datetime.fromtimestamp(
        os.path.getmtime(f"{TEMPLATES_DIR}/posts/{filename}")
    )
    updated = f"{updated_dt.isoformat(sep='T')}{TZINFO}"
    published_dt = parser.parse(soup.find("p", class_="date").get_text())
    published = f"{published_dt.isoformat(sep='T')}{TZINFO}"
    path = f"post/{filename[:filename.find('.')]}"
    author = "Shane Moore"
    content = escape(markup)
    return Post(title, updated, published, path, author, content)


def recents():
    posts = [
        extract_post_attributes(post_file)
        for post_file in os.listdir(f"{TEMPLATES_DIR}/posts/")
    ]
    return (
        render_template(
            "recent.atom.jinja2",
            posts=posts,
            last_updated=f"{dt.datetime.now().isoformat(sep='T')}{TZINFO}",
            url_root=os.environ["URL_ROOT"] or request.url_root,
        ),
        200,
        {"Content-Type": "application/atom+xml; charset=utf-8"},
    )
