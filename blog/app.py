from flask import Flask, render_template
from flask_assets import Environment
from jinja2.exceptions import TemplateNotFound
from .server_config import css_bundle, js_bundle, jinja_should_autoescape
from . import commands
from . import atom_feed

import click

app = Flask(__name__)

app.jinja_env.autoescape = jinja_should_autoescape

assets = Environment(app)
assets.register("css", css_bundle)
assets.register("js", js_bundle)


@app.cli.command("new-draft")
@click.option("--slug", "-s", required=True, type=str)
@click.option("--title", "-t", required=True, type=str)
def new_draft(slug: str, title: str):
    commands.new_draft(slug, title)


@app.cli.command("publish")
@click.option("--slug", "-s", required=True, type=str)
def publish(slug: str):
    commands.publish(slug)


@app.route("/")
def index():
    return render_template("index.html.jinja2")


@app.route("/about_fathom/")
def about_fathom():
    return render_template("about_fathom.html.jinja2")


if app.config["ENV"] == "development":

    @app.route("/draft/<draft_slug>/")
    def draft(draft_slug=""):
        try:
            return render_template(f"drafts/{draft_slug}.html.jinja2")
        except TemplateNotFound:
            return render_template("404.html.jinja2"), 404


@app.route("/post/<post_slug>/")
def post(post_slug=""):
    try:
        return render_template(f"posts/{post_slug}.html.jinja2")
    except TemplateNotFound:
        return render_template("404.html.jinja2"), 404


@app.route("/recent.atom")
def recent_feed():
    return atom_feed.recents()
