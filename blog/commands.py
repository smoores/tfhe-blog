from datetime import date
import os
from pathlib import Path


TEMPLATES_DIR = f"{Path(__file__).parent.absolute()}/templates"


def new_draft(slug: str, title: str):
    today = date.today()
    month = date.today().strftime("%b") + "" if today.month == 5 else "."
    day = today.strftime("%d").lstrip("0")
    year = today.strftime("%Y")
    with open(f"{TEMPLATES_DIR}/drafts/{slug}.html.jinja2", "w") as draft:
        draft.write(
            f"""{{% extends "base.html.jinja2" %}}
{{% block title %}}smoores.dev - {title}{{% endblock %}}
{{% block description %}}
  <!-- Add a description here. -->
{{% endblock %}}
{{% block main %}}
  <article>
    <div class="heading-underline">
      <h2>{title}</h2>
    </div>
    <p class="date">{ f"{month} {day}, {year}" }</p>
    <section id="intro">
      <p>
      </p>
    </section>
  </article>
{{% endblock %}}
"""
        )


def publish(slug: str):
    os.rename(
        f"{TEMPLATES_DIR}/drafts/{slug}.html.jinja2",
        f"{TEMPLATES_DIR}/posts/{slug}.html.jinja2",
    )
