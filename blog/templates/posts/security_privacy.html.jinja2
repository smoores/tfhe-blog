{% extends "base.html.jinja2" %}
{% block main %}
<article class="full-post">
  <div class="heading-underline">
    <h2>Security and Privacy</h2>
  </div>
  <p class="date">Jan. 3, 2019</p>
  <section id="intro">
    <p>
      I can’t say with confidence what exactly this blog will become, but according to the front page, it's about
      personal privacy on the Internet. But freezing your credit, you could argue, is largely a security measure; it
      won’t do anything to keep your financial history private. And data breaches can affect both privacy and security,
      depending on the type of data being revealed. Sometimes it can be difficult to find where the line is between
      these two concepts, but in order to continue having an informed conversation about digital privacy, security, and
      even safety, I’m going to attempt to find that line.
    </p>
    <p>
      This is about to get massively ideological. Part of me apologizes for that, but the rest of me is really excited.
    </p>
  </section>
  <section id="privacy">
    <h3>What is Privacy?</h3>
    <p>
      For me, the most helpful context for framing a conversation about personal privacy is actually the American Bill
      of Rights. More specifically, the text of the first amendment, which reads as follows:
    </p>
    <q>
      Congress shall make no law respecting an establishment of religion, or prohibiting the free exercise thereof; or
      abridging the freedom of speech, or of the press; or the right of the people peaceably to assemble, and to
      petition the Government for a redress of grievances.
    </q>
    <p>
      This is actually a substantially abbreviated version of what James Madison originally submitted:
    </p>
    <q>
      The civil rights of none shall be abridged on account of religious belief or worship, nor shall any national
      religion be established, nor shall the full and equal rights of conscience be in any manner, or on any pretext,
      infringed. The people shall not be deprived or abridged of their right to speak, to write, or to publish their
      sentiments; and the freedom of the press, as one of the great bulwarks of liberty, shall be inviolable. The
      people shall not be restrained from peaceably assembling and consulting for their common good; nor from applying
      to the Legislature by petitions, or remonstrances, for redress of their grievances.
    </q>
    <p>
      You’ll notice that even though the second text is almost twice as long as the first, and uses fairly different
      language, the order of the rights is identical. I actually prefer to look at the original draft because it’s a
      little bit more specific, and I think does an even better job of showing Madison’s intentions.
    </p>
    <p>
      The First Amendment can be roughly broken up into five clauses, and the order of those clauses is actually very
      important. They expand freedoms outward in logical succession, starting with the most personal:
    </p>
    <ol>
      <li>
        <h4>Establishment Clause</h4>
        <p>
          The first clause regards the personal practice of religion, and in the first draft we also see reference to
          “equal rights of conscience”. The very first right that Madison spells out is freedom of thought. He seems to
          be using religion as a proxy for internal values and beliefs, and Madison says that the government can never
          establish a structure that imposes a framework of values and beliefs on its citizens.
        </p>
      </li>
      <li>
        <h4>Free Exercise Clause</h4>
        <p>
          Not only does the first amendment prevent the government from imposing thoughts on its citizens, it barres it
          from in any way restricting the thoughts its citizens can have.
        </p>
      </li>
      <li>
        <h4>Freedom of Speech and of the Press</h4>
        <p>
          Now that he's established that citizens are free to think however they please, Madison moves outward. Those
          thoughts are meaningless if they can't be communicated to others, so that right needs to be protected as
          well. "The people shall not be deprived or abridged of their right to speak, to write, or to publish their
          sentiments", and even further, "the freedom of the press... shall be inviolable." Madison and his fellow
          Federalist, Thomas Jefferson, even believed that false statements critical of the government should be
          protected. When Jefferson was elected president in 1800, he allowed the Sedition Act of 1798 to expire and
          pardoned those imprisoned by it, believing that the act violated the First Amendment.
        </p>
      </li>
      <li>
        <h4>Peaceable Assembly</h4>
        <p>
          Now the trend is even more apparent. Not only can citizens have their owns thoughts, and communicate them to
          each other, they can meet en masse and have open discourse about these ideas, <em>even if they're critical of
            the government.</em>
        </p>
      </li>
      <li>
        <h4>The Petition Clause</h4>
        <p>
          Finally, if enough citizens feel strongly enough that the government has wronged them, they have the right to
          petition for redress of grievances. This is how democracy happens in this country. Calling your senator and
          attending your mayor's town hall, lobbying the federal government and filing lawsuits against it are all
          protected under this clause of the First Amendment.
        </p>
      </li>
    </ol>
    <p>
      This is all directly tied to the notion of personal privacy. These rights are the foundation of a free democracy,
      and they all cascade from a single thesis: "...nor shall the full and equal rights of conscience be in any
      manner, or on any pretext, infringed." But there's a hitch. People behave differently when they believe they're
      being watched. They speak differently, act differently, meet with different groups and have
      different tendencies to react to authority. Without the guarantee of personal privacy, the rights protected by
      the First Amendment are eroded. Each of the clauses above only works as intended with the additional
      qualification that they can be acted upon <em>in private.</em>
    </p>
    <p>
      These behavioral self-modifications are often referred to as "chilling effects". Researcher Jon Penney has written
      about the chilling effects of Snowden's revelations about the NSA's mass surveillance programs after noticing a
      <a href="https://ethos.bl.uk/OrderDetails.do;jsessionid=FDA131FA27FA2D59CE360D917111ACCC?uin=uk.bl.ethos.740786">
      sudden decline in Wikipedia searches for certain terrorism-related keywords.</a> Cybersecurity expert Bruce
      Schneier uses gay marriage as an example of progress that absolutely required personal privacy in order to
      take place. As he puts it, "It’s done by a few; it’s a counterculture; it’s mainstream in cities; young people
      don’t care anymore; it’s legal. And this is a long process that needs privacy to happen."
    </p>
    <p>
      Even if you personally have nothing to hide, even if you are completely, absolutely certain that no future
      government will ever have reason to see any of your actions, real or implied, as threatening or dissenting,
      this should still bother you. It bothers me. Because even if I haven't changed my own behavior, it's clear that
      others have, and democracies only work if <em>everyone</em> is able to freely and openly participate.
    </p>
    <p>
      This, for me, is the crux of the conversation about digital privacy. If a huge portion of communication,
      knowledge acquisition, and organization is done online, then the very foundations of a free and democratic
      society are threatened by every attempt to reduce personal privacy online. The more people are watched, and the
      more aware of that observation they become, the less likely they are to speak out or act in defiance of… well
      anything.
    </p>
  </section>
  <section>
    <h3>Enter, Security</h3>
    <p>
      A thoughtful reader might be asking, "Ok, that's nice and all, but what am I supposed to do about it?" What a
      great question, thoughtful reader. Let's talk about security.
    </p>
    <p>
      I tried to look up a good working definition of security, but unfortunately all Merriam-Webster had for me was
      "the state of being secure." Insightful. So let's build our own. In the context of personal freedom, security is
      about loss mitigation. Locks, therefore, are security measures. The complex system of custom bolts
      and u-locks I use to secure my bike in the city provides me some amount of security; it mitigates the risk of
      losing my bike. Passwords are also security measures. The password to my online banking portal
      mitigates the risk of losing my money.
    </p>
    <p>
      What about security of privacy? How can we secure our ability to have thoughts and conversation without risk of
      being spied upon? Without risk of those very thoughts, beliefs and opinions being used against us in some way?
    </p>
    <p>
      These risks are real. As I mentioned in <a href="/post/burden_of_understanding">my first post</a>, there are already companies seeking
      to use your very thoughts and feelings against you. The internet as it exists today, funded by personalized ads
      and directed by a small number of voices, sets us up for failure. Fighting for privacy means fighting against...
      Basically everyone. But it means fighting for democracy, and for personal freedom. And yes, I'm fully aware of
      how cheesy that sounds, but screw it, this is a call to arms. Let's make the Internet better.
    </p>
  </section>
</article>
{% endblock %}