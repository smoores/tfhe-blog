#!/usr/bin/env python
from flask_frozen import Freezer
from pathlib import Path
from dotenv import load_dotenv
from blog import app

load_dotenv(dotenv_path=".flaskenv")

app.config['FREEZER_DESTINATION'] = f"{Path().absolute()}/build"

freezer = Freezer(app)


if __name__ == "__main__":
    freezer.freeze()
